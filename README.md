Systemd-based docker images for molecule testing
================================================

This repository provides a small selection of systemd-based docker images to run tests against ansible playbooks using molecule.

Currently the following images are generated (on a monthly-basis):

 * rockylinux:9
 * ubuntu:24.04
 * debian:bookworm
 * fedora:42

The images are served via the builtin Gitlab docker registry under: `registry.gitlab.com/rocketduck/molecule-images/{image_spec}`, where `image_spec` refers to the listed images above.

If the system runs cgroups v2 with a current podman, one can use:

```yaml
  image: registry.gitlab.com/rocketduck/molecule-images/{image_spec}
  pull: true
  pre_build_image: true
  override_command: false
```

with the podman molecule driver. This even works rootless.

For docker please follow the instruction at https://molecule.readthedocs.io/en/latest/examples.html#systemd-container.
